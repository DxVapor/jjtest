'use strict';
module.exports = function(app) {
  var apiController = require('../controllers/apiController');

  app.route('/directory')
    .get(apiController.getDirectory);
};