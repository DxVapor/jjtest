# Clone Rep
```
git clone https://gitlab.com/DxVapor/jjtest.git DockerQuickExample
cd DockerQuickExample
```
# Docker instructions API
```
docker build -t api .
docker run -it -v /usr/src/app -v /usr/src/app/node_modules -p 3000:3000 --rm --name api api
```

# Docker Instructions UI
```
cd ui
docker build -t ui .
docker run -it -v /usr/src/app -v /usr/src/app/node_modules -p 4200:4200 --rm --name website ui
```
to run the docker containers seperately not in your console you can add the `-d` tag to it.


open in browser: http://localhost:4200

search directory '.' for everything

Search any directory result from initial search, to see contents I.E `Node_Modules` folder