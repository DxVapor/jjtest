"use strict";

const fs = require("fs"),
  path = require("path");



exports.getDirectory = (req, res) => {
  var directoryPath = path.normalize(req.query.path);
  console.log(directoryPath);
  if (!directoryPath) res.send("Path cannot be empty");
  fs.exists(directoryPath, exists => {
    if (!exists) res.send("Directory does not exist");
    else
      fs.readdir(directoryPath, (err, data) => {
        if (err) {
          res.send(err.message);
        }

        res.writeHead(200, {
            'Content-Type':'plain/text'
        });

        for(var i = 1; i <= data.length; i ++ ){
            if(i == 1){
                res.write('[')
            }

            var fullPath = path.resolve(directoryPath, data[i - 1]);
            var stats = fs.statSync(fullPath);

            var obj = {
                size: stats.size,
                isDirectory: stats.isDirectory(),
                path: fullPath,
                created: stats.ctime,
                modified: stats.mtime
              };

            res.write(JSON.stringify(obj) + (i < data.length ? ',' : ']' ));
        }

        res.end();
      });
  });
};
