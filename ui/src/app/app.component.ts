import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "jjtest";
  directory: string;
  subDirectoriesAndFiles: object[];
  errorText: string;

  constructor(private http: HttpClient) {
    console.log(environment.apiURL);
  }

  getDirectoryContents(path: string) {
    this.http
      .get<object[]>(environment.apiURL + "/directory?path=" + path)
      .toPromise()
      .then(data => {
        this.subDirectoriesAndFiles = data;
      })
      .catch(err => {
        this.errorText = err.error.text;
      });
  }

  getDirectories() {
    this.errorText = "";
    this.getDirectoryContents(this.directory);
  }
}
